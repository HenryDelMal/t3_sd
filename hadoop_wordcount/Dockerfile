# Use the official Ubuntu 22.04 image as the base image
FROM ubuntu:22.04

# Update the package repository and install necessary dependencies
RUN apt-get update && \
    apt-get install -y \
    openjdk-8-jdk \
    wget \
    python3 \
    python3-pip

# Set the JAVA_HOME environment variable for arm64
ENV JAVA_HOME=/usr/lib/jvm/java-8-openjdk-arm64
ENV PATH=$PATH:${JAVA_HOME}/bin

# Download and install Hadoop (adjust version as needed)
RUN wget https://downloads.apache.org/hadoop/common/hadoop-3.3.1/hadoop-3.3.1.tar.gz && \
    tar -xzf hadoop-3.3.1.tar.gz -C /opt/ && \
    rm hadoop-3.3.1.tar.gz

# Set environment variables for Hadoop
ENV HADOOP_HOME=/opt/hadoop-3.3.1
ENV PATH=$PATH:$HADOOP_HOME/bin:$HADOOP_HOME/sbin

# Install required Python packages
RUN pip3 install wikipedia-api

# Clean up
RUN apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Set the working directory
WORKDIR /app

# Copy your Python scripts or files into the container
COPY app /app

# Expose Hadoop ports (adjust as needed)
EXPOSE 50010 50020 50070 50075 50090 8020 9000 9870

# Start bash when the container runs
CMD ["/bin/bash"]
