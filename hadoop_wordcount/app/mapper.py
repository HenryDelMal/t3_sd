#!/usr/bin/python3

import sys
import re

def mapper():
    for line in sys.stdin:
        line = line.strip()
        words = line.split()

        for word in words:
            # Check if the word consists only of numbers using regex
            if not re.match(r"^\d+$", word):
                sys.stdout.write("{}\t1\n".format(word))

if __name__ == "__main__":
    mapper()