#!/bin/bash

# WordCount
hadoop jar $HADOOP_HOME/share/hadoop/tools/lib/hadoop-streaming-3.3.1.jar \
-files /app/mapper.py,/app/reducer.py \
-mapper mapper.py \
-reducer reducer.py \
-input input/*/*.txt \
-output wordcount-output