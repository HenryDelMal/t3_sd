#!/usr/bin/python3

import sys

def reducer():
    word_counts = {}
    for line in sys.stdin:
        line = line.strip()
        word, count = line.split("\t")
        count = int(count)
        if word in word_counts:
            word_counts[word] += count
        else:
            word_counts[word] = count
    for word, count in word_counts.items():
        sys.stdout.write("{}\t{}\n".format(word, count))

if __name__ == "__main__":
    reducer()
