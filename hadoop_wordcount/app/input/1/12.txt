12 (XII) fue un año bisiesto comenzado en viernes del calendario juliano, en vigor en aquella fecha. En el Imperio romano, era conocido como el Año del consulado de Germánico y Cayo Fonteyo Capitón (o menos frecuentemente, año 765 Ab urbe condita). La denominación 12 para este año ha sido usado desde principios del período medieval, cuando la era A. D. se convirtió en el método prevalente en Europa para nombrar a los años.

Acontecimientos
Annio Rufo es nombrado Prefecto de Judea.
Publio Sulpicio Quirinio regresa a Judea para convertirse en consejero de Tiberio.
Augusto ordena la mayor invasión de Germania más allá del Rin.

Nacimientos
31 de agosto: Calígula, emperador romano (m. 41).

Enlaces externos
 Wikimedia Commons alberga una categoría multimedia sobre 12.