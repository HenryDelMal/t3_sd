23 (XXIII) fue un año común comenzado en viernes del calendario juliano, en vigor en aquella fecha. En el Imperio romano, era conocido como el Año del consulado de Polio y Veto (o menos frecuentemente, año 776 Ab urbe condita). La denominación 23 para este año ha sido usado desde principios del período medieval, cuando la era A. D. se convirtió en el método prevalente en Europa para nombrar a los años.

Acontecimientos
Imperio Romano
14 de septiembre: Sejano toma control del Senado romano tras envenenar al cónsul Druso.[1]​
Estrabón publica su libro Geografía, compendio del saber cartográfico clásico. Es el único libro de la Antigüedad en haber sobrevivido hasta el presente.[2]​
El gobernador de la Bética, Numerio Vibio Sereno, es acusado de corrupción y más tarde desterrado.[3]​
Cayo Asinio Polión y Gayo Antistio Veto son cónsules en Roma.
Comienza el reinado de Ptolomeo, último rey de Mauritiania.

Imperio Chino
Liu Xuan, un rebelde descendiente de los Han, se proclama Emperador y se subleva junto a los Cejas Rojas contra Wang Mang y su Dinastía Xin.
4 de octubre: los rebeldes toman la capital de Chang'an.
6 de octubre: fin de la Dinastía Xin. China vivirá un breve periodo de anarquía hasta la restauración de la Dinastía Han en el año 25.

Nacimientos
Plinio el Viejo, historiador romano.

Fallecimientos
14 de septiembre: Druso Julio César, cónsul romano y nieto del emperador Tiberio.
6 de octubre: Wang Mang, emperador chino, fundador de la dinastía Xin.
Liu Xin, matemático, astrónomo, historiador y político chino.
Juba II, rey de Mauritania.

Referencias
Enlaces externos
 Wikimedia Commons alberga una categoría multimedia sobre 23.