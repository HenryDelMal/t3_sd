25 (XXV) fue un año común comenzado en lunes del calendario juliano, en vigor en aquella fecha. En el Imperio romano, era conocido como el Año del consulado de Léntulo y Agripa (o menos frecuentemente, año 778 Ab Urbe condita). La denominación 25 para este año ha sido usado desde principios del período medieval, cuando la era A. D. se convirtió en el método prevalente en Europa para nombrar a los años.

Acontecimientos
5 de agosto: Liu Xiu se proclama Emperador de China y restaura la Dinastía Han.[1]​
27 de noviembre: Liu Xiu mueve la capital imperial de Chang'an a Luoyang. En la historiografía china, este evento marca el inicio del periodo de Han Oriental.[1]​
Coso Cornelio Léntulo y Marco Asinio Agripa ejercen el consulado.
El emperador Tiberio resuelve una disputa entre Mesenia y Esparta sobre el Ager Dentheliales en el monte Taigeto, reconociendo la tierra a Mesenia.
Lucio Elio Sejano intenta sin éxito casarse con la viuda de Druso el Joven.

Nacimientos
Cayo Julio Civil, jefe germano.
Mesalina, emperatriz romana y esposa del emperador Claudio.

Fallecimientos
Liu Xuan, príncipe de la dinastía Han.

Arte y literatura
Pomponio Mela formaliza un sistema de zonas climáticas.

Referencias
Enlaces externos
 Wikimedia Commons alberga una categoría multimedia sobre 25.