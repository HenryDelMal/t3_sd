24 (XXIV) fue un año bisiesto comenzado en sábado del calendario juliano, en vigor en aquella fecha. En el Imperio romano, era conocido como el Año del consulado de  Cetego y Varrón (o menos frecuentemente, año 777 Ab urbe condita). La denominación 24 para este año ha sido usado desde principios del período medieval, cuando la era A. D. se convirtió en el método prevalente en Europa para nombrar a los años.

Acontecimientos
Fin de la dinastía Xin en China. La dinastía Han vuelve al poder.
Fin de la guerra que enfrentaba a Roma con Numidia y Mauritania.
Servio Cornelio Cetego y Luco Viselio Varrón son cónsules.
Charmides es arconte de Atenas.

Fallecimientos
Posible muerte de Estrabón (también puede ser en el 19).

Enlaces externos
 Wikimedia Commons alberga una categoría multimedia sobre 24.