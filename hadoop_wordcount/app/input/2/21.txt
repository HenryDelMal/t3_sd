21 (XXI) fue un año común comenzado en miércoles del calendario juliano, en vigor en aquella fecha. En el Imperio romano, era conocido como el Año del consulado de Augusto y César (o menos frecuentemente, año 774 Ab urbe condita). La denominación 21 para este año ha sido usado desde principios del período medieval, cuando la era A. D. se convirtió en el método prevalente en Europa para nombrar a los años.

Acontecimientos
Cuarto consulado de Tiberio Augusto y segundo de Druso Julio César.

Fallecimientos
Arminio el querusco (38), guerrero germano que destruyó al ejército romano.
Publio Sulpicio Quirino, gobernador romano de Siria.
Marco Valerio Mesala Barbado Mesalino, cónsul romano (o en el 20).

Enlaces externos
 Wikimedia Commons alberga una categoría multimedia sobre 21.