22 (XXII) fue un año común comenzado en jueves del calendario juliano, en vigor en aquella fecha. En el Imperio romano, era conocido como el Año del consulado de Agripa y Galba (o menos frecuentemente, año 775 Ab urbe condita). La denominación 22 para este año ha sido usado desde principios del período medieval, cuando la era A. D. se convirtió en el método prevalente en Europa para nombrar a los años.

Acontecimientos
Consulado de Gayo Sulpicio Galba.

Nacimientos
(dudoso) Valeria Mesalina, tercera esposa del emperador romano Claudio.

Fallecimientos
Junia Tercia, esposa de Cayo Casio Longino y hermana de Marco Junio Bruto.

Enlaces externos
 Wikimedia Commons alberga una categoría multimedia sobre 22.