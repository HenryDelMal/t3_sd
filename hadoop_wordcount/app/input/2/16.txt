16 (XVI) fue un año bisiesto comenzado en miércoles del calendario juliano, en vigor en aquella fecha. En el Imperio romano, era conocido como el Año del consulado de Tauro y Libón (o menos frecuentemente, año 769 Ab urbe condita). La denominación 16  para este año ha sido usado desde principios del período medieval, cuando la era A. D. se convirtió en el método prevalente en Europa para nombrar a los años.

Acontecimientos
Alemania: última batalla importante entre el general romano Germánico y el guerrero germano Arminio (17 a. C. - 21 d. C.), con grandes pérdidas de ambas partes, en Idistaviso (Angrivarierwall) cerca del río Weser.
Nóricos y panonios invaden Iliria, siendo rechazados por el procónsul Publio Silo.
Aparece la obra de Ovidio "Epistulae ex Ponto".

Nacimientos
A finales de año.— Julia Drusila, hija de Germánico y Agripina la Mayor.

Fallecimientos
Escribonia, quien había sido esposa de Augusto y era tía del cónsul en ejercicio, Lucio Escribonio Libón.

Enlaces externos
 Wikimedia Commons alberga una categoría multimedia sobre 16.