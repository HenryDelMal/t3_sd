11 (XI) fue un año común comenzado en jueves del calendario juliano, en vigor en aquella fecha. En el Imperio romano, era conocido como el Año del consulado de Lépido y Tauro (o menos frecuentemente, año 764 Ab urbe condita). La denominación 11 para este año ha sido usado desde principios del período medieval, cuando la era A. D. se convirtió en el método prevalente en Europa para nombrar a los años.

Acontecimientos
Imperio romano
Germánico asegura Germania Inferior y el Rin.
El emperador Augusto abandona su plan de crear una frontera defensiva en el Elba, para reforzar la defensa romana a lo largo del Rin y el Danubio.

Asia
Artabano II de la dinastía arsácida se convierte en gobernante de Partia.
En la India, Satakarni comienza su reinado como emperador de Andhra (11–29).

Nacimientos
Marco Antistio Labeón: jurista romano.

Enlaces externos
 Wikimedia Commons alberga una categoría multimedia sobre 11.