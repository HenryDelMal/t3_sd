14 (XIV) fue un año común comenzado en lunes del calendario juliano, en vigor en aquella fecha. En el Imperio romano, era conocido como el Año del consulado de Pompeyo y Apuleyo (o menos frecuentemente, año 767 Ab urbe condita). La denominación 14 para este año ha sido usado desde principios del período medieval, cuando la era A. D. se convirtió en el método prevalente en Europa para nombrar a los años.

Acontecimientos
Tiberio sucede a Augusto como emperador de Roma.
Revueltas en Germania por parte de las legiones romanas tras la muerte de Augusto. Comienza la guerra contra Arminio, acabará en el año 16.
El censo de población arroja 4.973.000 ciudadanos romanos.
Divinización de Augusto.
Insurrecciones en África.

Nacimientos
Salomé, princesa idumea.

Fallecimientos
19 de agosto - César Augusto, primer emperador de Roma.

Enlaces externos
 Wikimedia Commons alberga una categoría multimedia sobre 14.