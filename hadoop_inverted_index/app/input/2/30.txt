30 (XXX) fue un año común comenzado en domingo del calendario juliano, en vigor en aquella fecha. En el Imperio romano, era conocido como el Año del consulado de Vinicio y Longino (o menos frecuentemente, año 783 Ab urbe condita). La denominación 30 para este año ha sido usado desde principios del período medieval, cuando la era A. D. se convirtió en el método prevalente en Europa para nombrar a los años.

Acontecimientos
Imperio romano
Los romanos fundan la ciudad de Tournai en Bélgica.

India
Fundación del imperio kushan (fecha aproximada).

Cristianismo
Jueves 6 de abril: Jesucristo celebró la Santa Cena con sus discípulos al anochecer. De acuerdo a la Ley, la Pascua o Pesaj debía celebrarse durante siete días, del 7 al 14 de Abr il (Abib) en conmemoración de la salida de Egipto. Después salió con ellos al huerto de Los Olivos donde fue arrestado.
Viernes 7 de abril: El día siguiente por la mañana Jesucristo fue juzgado por el gobernador Poncio Pilato, remitido al rey Herodes y sentenciado por Pilato. Fue crucificado al mediodía. Murió a las 3 de la tarde. Su cuerpo fue bajado de la cruz y sepultado antes del inicio del Sabat (anochecer del Viernes). Estudios de varios exégetas fijan esta fecha el 14 de Nissan del calendario judío, coincidente con el 7 de abril del calendarios cristiano.[1]​[2]​[3]​[4]​
Domingo 9 de abril: Resurrección de Jesucristo. Por eso el Cristianismo instituyó el primer día de la semana, el Domingo (día del Señor) como su día sagrado y de descanso.
Según la tradición de la Iglesia católica, el apóstol Simón Pedro se convierte en su primer papa.

Arte y literatura
Gayo Julio Fedro traduce las fábulas de Esopo, y compone varias propias.
Veleyo Patérculo escribe una historia general de los países conocidos en la Antigüedad.

Nacimientos
Noviembre
8 de noviembre - Nerva, emperador romano.

Fechas desconocidas
Popea Sabina

Fallecimientos
7 de abril: ese día Jesucristo fue juzgado por la mañana por el gobernador Poncio Pilato, remitido al rey Herodes y de nuevo flagelado y sentenciado a muerte por Pilato. Fue crucificado al mediodía. Murió a las 3 de la tarde. Su cuerpo fue bajado de la cruz y sepultado antes del inicio del Sabat (anochecer del Viernes).

Referencias
Enlaces externos
 Wikimedia Commons alberga una categoría multimedia sobre 30.