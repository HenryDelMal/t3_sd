#!/usr/bin/python3

import sys
import re
import os

def mapper():
    # Get the current filename from the environment variable
    filename_with_extension = os.environ.get("mapreduce_map_input_file", "unknown")

    # Strip the ".txt" extension from the filename
    filename = int(os.path.splitext(filename_with_extension)[0].split('/')[-1])

    for line in sys.stdin:
        line = line.strip()
        words = line.split()

        for word in words:
            # Check if the word consists only of numbers using regex
            if not re.match(r"^\d+$", word):
                sys.stdout.write("{}\t({}, 1)\n".format(word, filename))

if __name__ == "__main__":
    mapper()