import wikipediaapi
wiki_wiki = wikipediaapi.Wikipedia(
    user_agent='University_Homework3/ver0.0',
        language='es',
        extract_format=wikipediaapi.ExtractFormat.WIKI
)

# Descarga documentos del 1 al 15
for i in range(1,16):
    article_name = str(i) # Ratamente usaremos el numero como articulo
    filename = "./input/1/"+article_name+".txt"
    file = open(filename, 'w')
    p_wiki = wiki_wiki.page(article_name)
    file.write(p_wiki.text)
    file.close()

# Descarga documentos del 16 al 30
for i in range(16,31):
    article_name = str(i) # Ratamente usaremos el numero como articulo
    filename = "./input/2/"+article_name+".txt"
    file = open(filename, 'w')
    p_wiki = wiki_wiki.page(article_name)
    file.write(p_wiki.text)
    file.close()

   