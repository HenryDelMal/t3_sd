#!/bin/bash
hadoop jar $HADOOP_HOME/share/hadoop/tools/lib/hadoop-streaming-3.3.1.jar \
-files mapper.py,json_reducer.py \
-mapper mapper.py \
-reducer json_reducer.py \
-input input/*/*.txt \
-output json_output