#!/usr/bin/python3

import sys
import json

def reducer():
    current_word = None
    current_counts = {}

    for line in sys.stdin:
        line = line.strip()
        word, info = line.split("\t")
        filename, count = info.strip('()').split(', ')
        count = int(count)

        if current_word == word:
            if filename in current_counts:
                current_counts[filename] += count
            else:
                current_counts[filename] = count
        else:
            if current_word:
                print_output(current_word, current_counts)
            current_word = word
            current_counts = {filename: count}

    if current_word:
        print_output(current_word, current_counts)

def print_output(word, counts):
    # Convert the counts dictionary to a list of dictionaries
    json_counts = [{"filename": file, "count": count} for file, count in counts.items()]

    # Convert the list of dictionaries to JSON format
    json_output = json.dumps({"word": word, "counts": json_counts})

    sys.stdout.write(f"{json_output}\n")

if __name__ == "__main__":
    reducer()