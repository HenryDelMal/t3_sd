#!/usr/bin/python3

import sys

def mapper():
    for line in sys.stdin:
        line = line.strip()
        sys.stdout.write(line + '\n')

if __name__ == "__main__":
    mapper()