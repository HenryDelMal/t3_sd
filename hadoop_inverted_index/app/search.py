import json

def search_word_occurrences(word, json_file_path):
    with open(json_file_path, 'r') as json_file:
        # Load the JSON data
        data = [json.loads(line) for line in json_file]

        # Find the word in the data
        matching_entries = [entry for entry in data if entry['word'] == word]

        if matching_entries:
            # Sort the occurrences by count in descending order
            occurrences = sorted(matching_entries[0]['counts'], key=lambda x: x['count'], reverse=True)

            # Show the top 5 occurrences
            top_5_occurrences = occurrences[:5]

            print(f"\nTop 5 occurrences of '{word}':")
            for entry in top_5_occurrences:
                print(f"File: {entry['filename']}.txt, Count: {entry['count']}, URL: https://es.wikipedia.org/wiki/{entry['filename']}")
        else:
            print(f"\nThe word '{word}' does not exist in the data.")

if __name__ == "__main__":
    word_to_search = input("Enter the word to search for: ")
    json_file_path = "json_output/part-00000"  # Replace with the actual path to your JSON file

    search_word_occurrences(word_to_search, json_file_path)